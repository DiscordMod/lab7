public class Board
{
	private Square[][] tictactoeBoard;
	private Square b = Square.BLANK;
	
	public Board()
	{
		tictactoeBoard = new Square[][]
		{
			{b, b, b},
			{b, b, b},
			{b, b, b}
		};
		
	}
	public String toString()
	{
		String board = "";
		for(int row = 0; row < 3; row++){
			for(int col = 0; col < 3; col++){
				board+=tictactoeBoard[row][col] + " ";
			}
			
			board+= "\n";
		}
		return board;
	}
	public boolean placeToken(int row, int col, Square playerToken)
	{
		if((row < 0 || row > 2) || (col < 0 || col > 2)){
			return false;
		}
		
		if(tictactoeBoard[row][col].equals(b)){
			tictactoeBoard[row][col] = playerToken;
				
				return true;
			
			}else{
			
				return false;
			}
	}
	public boolean checkIfFull()
	{
		for(int row = 0; row < 3; row++){
			for(int col = 0; col < 3; col++){
				if(tictactoeBoard[row][col].equals(b)){
					
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		for(int row = 0; row < 3; row++){
			int count = 0;
			for(int col = 0; col < 3; col++){
				if(tictactoeBoard[row][col] == (playerToken)){
					count++;
				}
				if(count == 3){
					return true;
				}
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Square playerToken)
	{
		
		for(int row = 0; row < 3; row++){
			int count = 0;
			for(int col = 0; col < 3; col++){
				if(tictactoeBoard[col][row] == (playerToken)){
					count++;
				}
				if(count == 3){
					return true;
				}
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true){
			return true;
		}
		return false;
	}
}